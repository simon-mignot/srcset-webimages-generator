const fs = require('fs-extra');
const exec = require('child_process').execSync;
const path = require('path');
const argv = require('minimist')(process.argv.slice(2));

if(typeof(argv.sizes) === 'undefined' && typeof(argv.S) === 'undefined'
    || typeof(argv.indir) === 'undefined' && typeof(argv.I) === 'undefined'
    || typeof(argv.outdir) === 'undefined' && typeof(argv.O) === 'undefined')
{
    return;
}

const conf =
{
    sizes: argv.sizes ? argv.sizes.split(',') : argv.S.split(','),
    indir: argv.indir || argv.I,
    outdir: argv.outdir || argv.O,
    rmOutdir: argv['rm-outdir'] || argv.R || false
};

/* Delete outdir if requested */
if(conf.rmOutdir)
    fs.removeSync(conf.outdir);

/* Resize Images */
const filenames = fs.readdirSync(conf.indir);
const filenamesString = filenames.map(function(n) { return path.join(conf.indir, n); }).join(' ');
const identified = toJson(exec('identify -format "\\"%i\\": %w,\\n" ' + filenamesString), 2);

for(let name in identified)
{
    if(!identified.hasOwnProperty(name))
        continue;
    let imgSize = identified[name];
    copyOriginal(name, imgSize);
    resize(name, imgSize);
}


/* Optimize Images */
let files = fs.readdirSync(conf.outdir);
let filesSize = toJson(exec('find ' + conf.outdir + ' -type f -printf \'"%p": %s,\\n\''), 2);
console.log(filesSize);

/* Jpg optimization as recommended by Google */
exec("convert " + path.join(conf.outdir, "*.jpg") + " -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB -set filename:t '%t' \"" + path.join(conf.outdir, "%[filename:t].min.jpg") + "\"", { shell: '/bin/bash' });
//exec("convert \"" + path.join(conf.outdir, "*.[Jj][Pp]?(E|e)[Gg]") + "\" -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace sRGB -set filename:t '%t' \"" + path.join(conf.outdir, "%[filename:t].min.jpg") + "\"", { shell: '/bin/bash' });
exec('cd ' + conf.outdir + ' && rm ' + files.join(' '));
let filesSizeMin = toJson(exec('find ' + conf.outdir + ' -type f -printf \'"%p": %s,\\n\''), 2);





function toJson(output, commaOffset)
{
    return JSON.parse('{' + output.slice(0, -commaOffset) + '}');
}

function copyOriginal(name, imgSize)
{
    let tmp = name.split('/').pop().split('.');
    tmp.splice(-1, 0, 'o');
    tmp.splice(-1, 0, imgSize);
    
    fs.copySync(name, path.join(conf.outdir, tmp.join('.')));
}
function resize(name, imgSize)
{
    let resizeCmd = "convert " + name;
    for(let sizeID in conf.sizes)
    {
        sizeID = parseInt(sizeID);
        let size = conf.sizes[sizeID];
        if(size > imgSize)
            continue;
        
        let tmp = name.split('.');
        tmp.splice(-1, 0, size);
        tmp = tmp.join('.').split('/').pop();
        if(sizeID === conf.sizes.length - 1 || conf.sizes[sizeID + 1] > imgSize)
            resizeCmd += " -resize " + size + " " + path.join(conf.outdir, tmp);
        else
            resizeCmd += " \\( +clone -resize " + size + " -write " + path.join(conf.outdir, tmp) + " +delete \\)";
    }
    exec(resizeCmd);
}